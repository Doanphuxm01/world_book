<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Book;
use Illuminate\Support\Facades\DB;
class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Books = Book::getAllBook();
        return view('admin.layout.ListBook.List',['Books' => $Books]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $Books = Book::getAllBook();
        return Response()->json(['Books' => $Books]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $obj = [];
        $obj['name'] = request('name');
        $obj['author'] = request('author');
        $obj['status'] = request('status');
        $obj['detail'] = request('detail');
        // dd($obj['author']);
        $save = [
            'name' => isset($obj['name']) ? $obj['name'] : '',
            'detail' => isset($obj['detail']) ? $obj['detail'] : '',
            'author' => isset($obj['author']) ? $obj['author'] : '',
            'status' => isset($obj['status']) ? $obj['status'] : '',
        ];
        $id = Book::insertGetId($save);
        // dd($id);
        $Books = Book::getAllBook();
        return response()->json(['data' => $Books,'success' => 'add success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $Books = Book::find($id);

        return response()->json($Books);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $Books = Book::find($id)->update($request->all());
        return response()->json([$Books,'success' => 'edit success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $Books = Book::findOrFail($id);
        $Books->delete();
        return response()->json($Books);
    }
}
