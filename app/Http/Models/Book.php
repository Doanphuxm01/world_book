<?php

namespace App\Http\Models; 

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'listbook';
    protected $fillable = [
        'name','detail','author','status'
    ];

    public static function getAllBook() {
        $conn = [['status', '>', 0]];  // status > 1  show
        return self::where($conn)->get();
    }

    public static function getAllBookById($id) {
        $conn = [['status', '>', 0]];       // status > 1 show detail
        $conn[] = ['id',$id];       // status > 1 show 
        return self::where($conn)->first();
    }
}
