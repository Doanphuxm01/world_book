
        $('#save').show();
        $('#update').hide();
        $('.myid').hide();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        function viewData() {
            $.ajax({
                type: "GET",
                dataType: "json",
                url: "admin/book/create",
                success: function (response) {
                    var rows = "";
                    $.each(response.Books, function (key,value) {
                        rows = rows + "<tr>";
                            rows = rows + "<td><a href='admin/detail/" + value.id + "'>" + value.name + "</a></td>";
                            rows = rows + "<td>" + value.detail + "</td>";
                            rows = rows + "<td>" + value.author + "</td>";
                            rows = rows + "<td>" + value.status + "</td>";
                            rows = rows + "<td width='200'>";
                            rows = rows + "<button type='button' class='btn btn-info'  onclick='editData(" + value.id + ")'>sửa</button>";
                            rows = rows + "<button type='button' style='margin-left: 36px' class='btn btn-danger' onclick='deleteData(" + value.id + ")'>xóa</button>";
                            rows = rows + "</td>";
                        rows = rows + "</tr>";
                    });
                    $('tbody').html(rows);
                }
            })
        }
        viewData();
        function ClearData() {
            $('#id').val('');
            $('#name').val('');
            $('#detail').val('');
            $('#author').val('');
        }
        function AddData() {
            $('#error').hide();
            $('#error2').hide();
            $('#error3').hide();
            var name = $('#name').val();
            var detail = $('#detail').val();            
            var author = $('#author').val();
            var status = $('#status').val();
            // let obj = [name, detail, author, status];
            $.ajax({
                type:'POST',
                dataType:'json',
                data: {name:name,detail:detail,author:author,status:status},
                url: "admin/book",
                success:function(response){
                    viewData();
                    toastr.success(response.success,'Thông báo',{timeOut: 2000});
                    viewData();
                    ClearData();
                    $('#save').show();
                }
            })
        }
        function editData(id) {
            $('#save').hide();
            $('#update').show();
            $('.myid').show();
            $.ajax({
                type: "GET",
                dataType: "json",
                url: "admin/book/" + id + "/edit",
                success: function (response) {
                    $('#id').val(response.id);
                    $('#name').val(response.name);
                    $('#detail').val(response.detail);
                    $('#author').val(response.author);
                    $('#status').val(response.status);
                    $('tbody').html();
                }
            })
        }
        function updateData() {
            var id = $('#id').val();
            var name = $('#name').val();
            var booktype = $('#booktype').val();
            var detail = $('#detail').val();
            var author = $('#author').val();
            $.ajax({
                type: "PATCH",
                dataType: "json",
                data: {name: name, id_book: booktype, detail: detail, author: author},
                url: 'admin/book/'+id,
                success: function (response) {
                    toastr.success(response.success, 'Thông báo', {timeOut: 2000});
                    viewData();
                    ClearData();
                    $('#save').show();
                    $('#update').hide();
                    $('.myid').hide();

                }
            })
        }
        function deleteData(id) {
            $.ajax({
                type: "DELETE",
                dataType: "json",
                url: "admin/book/" + id,
                success: function (response) {
                    toastr.success(response.success, 'Thông báo', {timeOut: 2000});
                    viewData();
                }
            })
        }
